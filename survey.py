#!/usr/bin/env python3
import praw
import csv
import re
r = praw.Reddit("Survey Parser 0.9 by PeopleCallMeChief")
submission = r.get_submission(submission_id="2bm5cu")
comments = submission.comments
survey = []
counter = 1
substitutions = [
    ('\xe9','e'),
    ('&amp;','&'),
    ('&amp;','&'),
    (re.compile(r'\([^)]*\)'),''),
    (re.compile(r'[-,/] .+$'),''),
    ("'",''),
    ('cream',''),
    ('soaps?',''),
    ('shav(e|ing)',''),
    ('stick',''),
    ('razors? |$',''),
    ('stropp?e','strop'),
    ('the',''),
    ('b&m','barister & mann'),
    (' and ',' & '),
    ('vdh','van der hagen'),
    ('tobs.*', 'taylor of old bond street'),
    ("trumpers","geo f. trumper"),
    ('mwf','mitchells wool fat'),
    (r' (co\.)|(company).*$',''),
    (r'col\.?(onel)? ',''),
    ('want monica bay rum',''),
    (r'^\s*((la)|(los angeles))\s*$','lass'),
    ('barister','barrister'),
    ('art of','aos'),
    ('how to grow a mustache','htgam'),
    ('synergy','htgam'),
    ('p.160','p160'),
    ('prorasso','proraso'),
    ]
starters = [
    'barrister & mann',
    'strop shoppe',
    'queen charlotte',
    'conk',
    'van der hagen',
    'mystic water',
    'musgo',
    'nivea',
    'hardy',
    'cyril r. salter',
    'craftsman',
    'crabtree & evelyn',
    'htgam',
    'aos',
    'lass',
    'proraso',
    'razorock',
    ]

for s in starters:
    substitutions.append(('^' + s + '.*',s))

skip_keywords = [
    'price',
    'waaay',
    'long are you',
    'missing some',
    'gotta get',
    'just checked',
    '[lL]ist:',
    '[Mm]ine:',
    'regular basis',
    'top is best',
    'has used',
    'works out alright',
    ]
for c in comments:
    lines = c.body.splitlines()
    if "*" in c.body:
        temp = list(filter(lambda x: "*" in x, lines))
        soaps = list(map(lambda x: x.replace('* ',''), temp))
    elif re.search('\n- ',c.body) is not None:
        temp = list(filter(lambda x: re.search(r'^- ', x) is not None, lines))
        soaps = list(map(lambda x: re.sub(r'^- ','',x), temp))
    else:
        soaps = lines
    soaps = list(map(lambda x: x.lower(), soaps))
    for pat,sub in substitutions:
        soaps = list(map(lambda x: re.sub(pat,sub,x),soaps))
    for w in skip_keywords:
        soaps = list(filter(lambda x: re.search(w,x) is None, soaps))
    soaps = list(map(lambda x: x.strip(),soaps))
    soaps = list(filter(lambda x: len(x) > 0,soaps))
    if len(soaps) > 0:
        if c.author:
            author = c.author.name
        else:
            author = 'Unknown Author ' + str(counter)
            counter+=1
        entry = [author] + soaps
        survey.append(entry)

with open('survey.csv','w') as csvfile:
    w = csv.writer(csvfile)
    for e in survey:
        w.writerow(e)
